using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationPiece : MonoBehaviour
{
    [SerializeField] private float speedRotation = 5f;
    [SerializeField] private Vector3 rotationSence = new Vector3(0, 0, 1);

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotationSence * speedRotation * Time.deltaTime);
    }

}
