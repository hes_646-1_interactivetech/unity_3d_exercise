using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transporter : MonoBehaviour
{
    Transform parentPlayer;

    private void Start()
    {
        parentPlayer = GameObject.FindGameObjectWithTag("Player").transform.parent;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.parent = transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.transform.parent = parentPlayer;
        }
    }

}
