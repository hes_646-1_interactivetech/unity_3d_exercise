using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustHeight : MonoBehaviour
{

    [SerializeField] private float hight = 1;
    private bool isInitialized = false;
    [SerializeField] private Vector3 originalPosition;
    [SerializeField] private Vector3 floorHight;

    // Start is called before the first frame update
    void Start()
    {
        originalPosition = transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        if (!isInitialized)
        {
            isInitialized = true;
            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit))
            {
                floorHight = hit.point;
            }

            transform.position = new Vector3(transform.position.x, floorHight.y + hight, transform.position.z);
        }


    }
}
