using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiate : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject prefab;
    private Transform contentSpwan;

    // Start is called before the first frame update
    void Start()
    {
        contentSpwan = transform;

        foreach (Transform child in contentSpwan)
        {
            GameObject clone = Instantiate(prefab, child.position, prefab.transform.rotation);
            clone.transform.SetParent(child);
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
