using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Respawn : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform spwanPoint;

    [SerializeField] private TextMeshProUGUI counterLabel = default;
    private int maxVies = 3;
    private int counterVies = 3;

    private void Start()
    {
        counterVies = maxVies;
        counterLabel.text = "Vies : " + counterVies + " / " + maxVies;
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            counterVies--;

            counterLabel.text = "Vies : " + counterVies + " / " + maxVies;
            if(counterVies == 0)
            {
                SceneManager.LoadScene(0);
            }
            other.transform.position = spwanPoint.transform.position;
        }
    }

}
