using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CollisionPiece : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //Search in parent the second child being actually an audiosource. this.transform.parent.GetChild(1).gameObject.GetComponent<AudioSource>().Play();
           GameObject.FindObjectOfType<PieceCounter>().AddPiece();
            //Destroy GameObject with delay
            Destroy(this.transform.parent.gameObject, 0.3f);
        }
    }

}
