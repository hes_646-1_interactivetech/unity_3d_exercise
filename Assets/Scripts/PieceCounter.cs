using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PieceCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI counterLabel = default;
    [SerializeField] private GameObject victoryPanel = default;
    [SerializeField] private Transform pieceSpawns = default;
    private int counterPiece = 0;
    private int maxPiece = 4;

    // Start is called before the first frame update
    void Start()
    {
        victoryPanel.SetActive(false);
        maxPiece = pieceSpawns.childCount;
        counterLabel.text = counterPiece + " / " + maxPiece;
    }

    public void AddPiece()
    {
        counterPiece++;
        counterLabel.text = counterPiece + " / " + maxPiece;
        if (counterPiece == maxPiece)
        {
            victoryPanel.SetActive(true);
        }
    }

}
